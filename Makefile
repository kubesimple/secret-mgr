default: build

build:
	./build-tools/scripts/build

run:
	./bin/secret-mgr_darwin_amd64 --loglvl debug

check_fmt:
	./build-tools/scripts/check

release:
	docker build -t jharshman/secret-mgr .
	# docker login -u $(DOCKER_USER) -p $(DOCKER_PASSWORD)

install:
	./build-tools/scripts/install -c $(CTX) -n $(NS)

clean:
	rm -f ./bin/*
