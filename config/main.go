package config

import (
	"fmt"
	log "github.com/sirupsen/logrus"
)

type Config struct {
	Port   string `mapstructure:"port"`
	LogLvl string `mapstructure:"loglvl"`
	Vault  string `mapstructure:"vault"`
}

func (c *Config) BindPort() string {
	return fmt.Sprintf(":%s", c.Port)
}

func (c *Config) LogLevel() log.Level {
	var l log.Level
	switch c.LogLvl {
	case "info":
		l = log.InfoLevel
	case "warn":
		l = log.WarnLevel
	case "error":
		l = log.ErrorLevel
	case "debug":
		l = log.DebugLevel
	default:
		l = log.InfoLevel
	}
	return l
}
