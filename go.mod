module bitbucket.org/kubesimple/secret-mgr

require (
	github.com/golang/protobuf v1.3.1
	github.com/hashicorp/vault v1.0.3
	github.com/jharshman/vaultutil v0.0.2
	github.com/mitchellh/go-homedir v1.1.0
	github.com/sirupsen/logrus v1.4.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.2
	golang.org/x/net v0.0.0-20190320064053-1272bf9dcd53
	google.golang.org/grpc v1.19.1
)
