package grpc

import (
	"bitbucket.org/kubesimple/secret-mgr/config"
	"bitbucket.org/kubesimple/secret-mgr/pkg/grpc/api"
	log "github.com/sirupsen/logrus"
	rpc "google.golang.org/grpc"
	"net"
	"os"
	"os/signal"
	"syscall"
)

func Start(cfg *config.Config) error {

	initLogger(cfg.LogLevel())

	lis, err := net.Listen("tcp", cfg.BindPort())
	if err != nil {
		return err
	}
	defer lis.Close()

	srv := &handler{
		vaultAddr: cfg.Vault,
	}
	grpcServer := rpc.NewServer()
	api.RegisterSecretManagerServer(grpcServer, srv)

	done := make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGINT, syscall.SIGTERM)

	log.Infof("starting grpc server: %s\n", cfg.BindPort())
	go grpcServer.Serve(lis)

	<-done
	grpcServer.Stop()
	log.Infof("stopped server\n")

	return nil
}

func initLogger(level log.Level) {
	log.SetLevel(level)
}
