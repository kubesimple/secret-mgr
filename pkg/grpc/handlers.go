package grpc

import (
	"bitbucket.org/kubesimple/secret-mgr/pkg/grpc/api"
	"context"
	"errors"
	vault "github.com/hashicorp/vault/api"
	"github.com/jharshman/vaultutil"
)

type handler struct {
	vaultAddr string
}

func (h *handler) Read(ctx context.Context, in *api.RequestForSecret) (*api.Secrets, error) {
	vClient, err := vault.NewClient(vault.DefaultConfig())
	if err != nil {
		return &api.Secrets{}, err
	}
	vClient.SetAddress(h.vaultAddr)

	vLogical := vaultutil.Conn{
		Client: vClient.Logical(),
	}

	s, err := vLogical.Auth(vaultutil.Github(in.Token))
	if err != nil {
		return &api.Secrets{}, err
	}

	vClient.SetToken(s.ClientToken)

	raw, err := vLogical.Client.Read(in.Path)
	if err != nil {
		return &api.Secrets{}, err
	}
	if raw.Data == nil {
		return &api.Secrets{}, err
	}

	data, ok := raw.Data["data"].(map[string]interface{})
	if !ok {
		return &api.Secrets{}, errors.New("type assertion failed, could not convert to map")
	}

	secretArray := make([]*api.Secret, len(data))
	i := 0
	for k, v := range data {
		secretArray[i] = &api.Secret{
			Key:   k,
			Value: v.(string),
		}
		i++
	}

	return &api.Secrets{
		Data: secretArray,
	}, nil
}
