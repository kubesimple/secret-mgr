/*
Copyright © 2019 Joshua Harshman

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"bitbucket.org/kubesimple/secret-mgr/config"
	"bitbucket.org/kubesimple/secret-mgr/pkg/grpc"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"os"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var (
	cfgFile string
	c       config.Config
	port    string
	loglvl  string
	vault   string
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "secret-mgr",
	Short: "A small service providing access to vault secrets",
	Long: `
Secret-Mgr is a lightweight service that provides access
to Vault secrets over gRPC.`,
	Run: func(cmd *cobra.Command, args []string) {
		if err := viper.Unmarshal(&c); err != nil {
			log.Errorf("unable to unmarshal configuration: %v\n", err)
			return
		}
		grpc.Start(&c)
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.secret-mgr.yaml)")
	rootCmd.Flags().StringVar(&port, "port", "9000", "grpc bind port")
	rootCmd.Flags().StringVar(&loglvl, "loglvl", "info", "log level")
	rootCmd.Flags().StringVar(&vault, "vault", "http://127.0.0.1:8200", "vault address and port")

	viper.BindPFlags(rootCmd.Flags())
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		viper.AddConfigPath(home)
		viper.SetConfigName(".secret-mgr")
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
